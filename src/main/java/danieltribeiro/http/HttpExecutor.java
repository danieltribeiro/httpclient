package danieltribeiro.http;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**sponsible for making http calls and return the response
 * This class is re
 */
public class HttpExecutor {

  private HttpClient client = HttpClientBuilder.create().build();

  /**
   * Executa a http call
   * @param method The method GET, PUT, POST, PATCH, HEAD, OPTINS
   * @param url The complete url
   * @param headers optional List of headers
   * @param body optional Body
   * @return A HttpResponse
   */
  public HttpResponse executeMethod(String method, String url, Map<String, String> headers, byte[] body) {

    HttpRequestBase request;
    switch (method) {
    case "GET":
      request = new HttpGet();
      break;
    case "POST":
      request = new HttpPost();
      break;
    case "PUT":
      request = new HttpPut();
      break;
    case "PATCH":
      request = new HttpPatch();
      break;
    case "OPTIONS":
      request = new HttpOptions();
      break;
    case "HEAD":
      request = new HttpHead();
      break;
    default:
      throw new RuntimeException("Method not found: " + method);
    }

    if (headers != null) {
      for (Entry<String, String> e : headers.entrySet()) {
        request.addHeader(e.getKey(), e.getValue());
      }
    }

    if (body != null) {
      try {
        ((HttpEntityEnclosingRequest) request).setEntity(new ByteArrayEntity(body));
      } catch (Exception e) {
        throw new RuntimeException("Error parsing body: " + e.getMessage());
      }
    }

    try {
      request.setURI(new URI(url));
      org.apache.http.HttpResponse responseFromHost = client.execute(request);
      
      HttpResponse response = new HttpResponse();
      response.setCode(responseFromHost.getStatusLine().getStatusCode());
      response.setReason(responseFromHost.getStatusLine().getReasonPhrase());
      response.setBody(EntityUtils.toString(responseFromHost.getEntity()));
      EntityUtils.consumeQuietly(responseFromHost.getEntity());
      
      Map<String, String> responseReaders = new HashMap<>();
      Header[] headersFromHost = responseFromHost.getAllHeaders();
      for (Header h : headersFromHost) {
        responseReaders.put(h.getName(), h.getValue());
      }
      return response;
    } catch (Exception e) {
      throw new RuntimeException("Error executing http call: " + e.getMessage());
    }
  }
}