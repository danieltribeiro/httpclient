package danieltribeiro.http;

import java.util.Map;

public class HttpClient {

  private HttpExecutor executor;

  private String baseUrl = "";

  public HttpClient() {
    executor = new HttpExecutor();
  }

  public HttpClient(String baseUrl) {
    executor = new HttpExecutor();
    this.baseUrl = baseUrl;
  }

  /**
   * @return the executor
   */
  public HttpExecutor getExecutor() {
    return executor;
  }

  /**
   * @param executor the executor to set
   */
  public HttpClient setExecutor(HttpExecutor executor) {
    this.executor = executor;
    return this;
  }

  /**
   * @return the baseUrl
   */
  public String getBaseUrl() {
    return baseUrl;
  }

  /**
   * @param baseUrl the baseUrl to set
   */
  public HttpClient setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
    return this;
  }

  public HttpResponse execute(String method, String path, Map<String, String> headers, byte[] body) {
    return executor.executeMethod(method, concatenateUrl(path), headers, body);
  }

  public String concatenateUrl(String path) {
    return concatenateUrl(baseUrl, path);
  }

  public static String concatenateUrl(String url, String path) {
    String conc = url;
    if (url.endsWith("/")) {
      conc = url;
    } else {
      conc = url + "/";
    }

    if (path.startsWith("/")) {
      return (conc + path.substring(1));
    } else {
      return (conc + path);
    }
  }
  
  public static void main(String[] args) {
    String s = new HttpClient("http://google.com?q=teste").execute("GET", "/", null, null).getBody();
    System.out.println(s);
  }
}