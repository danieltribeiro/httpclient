package danieltribeiro.http;

import java.nio.charset.Charset;
import java.util.Map;

import org.apache.http.Header;

/**
 * The response of a http execute
 */
public class HttpResponse {
  
  private int code;

  private String body;

  private String reason;

  private Map<String, String> headers;

  /**
   * Verify if the response is positive
   * @return true if the response was 2XX HTTP Success codes
   */
  public boolean isOk() {
    return (code >= 200 && code < 300);
  }
  
  /**
   * @return the body
   */
  public String getBody() {
    return body;
  }

  /**
   * @param body the body to set
   */
  public void setBody(String body) {
    this.body = body;
  }

  /**
   * @return the code
   */
  public int getCode() {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode(int code) {
    this.code = code;
  }

  /**
   * @return the headers
   */
  public Map<String, String> getHeaders() {
    return headers;
  }

  public String getHeader(String name) {
    return headers.get(name);
  }

  /**
   * @param headers the headers to set
   */
  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
  }

  /**
   * @return the reason
   */
  public String getReason() {
    return reason;
  }

  /**
   * @param reason the reason to set
   */
  public void setReason(String reason) {
    this.reason = reason;
  }
}